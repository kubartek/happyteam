import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { Player } from '../core/model/player';
import { FiringBoardComponent } from '../firing-board/firing-board.component';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnChanges {

  @Input() player!: Player;
  @Input() index!: number;
  @ViewChild('firingBoard') firingBoard!: FiringBoardComponent;

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.player != null){
      console.log('Player {0} ready.', this.index);
    }
  }

  public shoot(round: number): void {
    this.firingBoard.shoot(this.player.shots[round]);
  }
}
