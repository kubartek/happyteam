import { Component, OnChanges, OnInit, QueryList, SimpleChanges, ViewChildren } from '@angular/core';
import { interval, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { GameClientService } from '../core/services/game/game-client.service';
import { PlayerComponent } from '../player/player.component';
import { GameService } from './game.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit, OnChanges {

  @ViewChildren('players') players = new QueryList<PlayerComponent>();
  public game = this._gameClientService.result$.asObservable();
  protected unsubscribe$ = new Subject();
  public summaryText = '';


  constructor(private _gameClientService: GameClientService,
              private _gameService: GameService) {
  }

  ngOnInit(): void {

    this._gameClientService.result$.subscribe(game => {

      this._gameService.startGame();

      setTimeout(() => {
        interval(150).pipe(takeUntil(this.unsubscribe$)).subscribe(
          t => {
            const player1 = this.players.toArray()[0];
            const player2 = this.players.toArray()[1];

            if (t >= game[0].shots.length - 1 && t >= game[1].shots.length - 1) {
              this._gameService.setSummary('Tie!');
            } else if (t >= game[1].shots.length - 1) {
              this._gameService.setSummary('Player 2 wins!');
            } else if (t >= game[0].shots.length - 1) {
              this._gameService.setSummary('Player 1 wins!');
            }
            player1.shoot(t);
            player2.shoot(t);

            if (t >= game[0].shots.length - 1 || t >= game[1].shots.length - 1) {
              this._gameService.endGame();
              this.unsubscribe$.next();
            }
          });
      }
      );
    }
    );
    this._gameClientService.getGame();
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(this.players);
  }

}
