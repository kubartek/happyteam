import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  private readonly title = 'BattleShip Game';
  public isGameInProgress$ = new Subject();
  public summary$ = new BehaviorSubject(this.title);

  constructor() { }

  public startGame(): void {
    this.isGameInProgress$.next(true);
    this.summary$.next(this.title);
  }

  public endGame(): void {
    this.isGameInProgress$.next(false);
  }

  public setSummary(text: string): void {
    this.summary$.next(text);
  }
}
