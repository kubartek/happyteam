import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PlayerComponent } from './player/player.component';
import { BoardComponent } from './board/board.component';
import { FiringBoardComponent } from './firing-board/firing-board.component';
import { GameComponent } from './game/game.component';
import { CoreModule } from './core/core.module';

import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { MatToolbarModule } from '@angular/material/toolbar';

@NgModule({
  declarations: [
    AppComponent,
    PlayerComponent,
    BoardComponent,
    FiringBoardComponent,
    GameComponent,
    ToolbarComponent
  ],
  imports: [
    BrowserModule,
    CoreModule,
    MatGridListModule,
    MatCardModule,
    MatToolbarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
