import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ShotType } from '../core/consts/ShotType';
import { TileType } from '../core/consts/TileType';
import { Shot } from '../core/model/shot';
import { Tile } from '../core/model/tile';

@Component({
  selector: 'app-firing-board',
  templateUrl: './firing-board.component.html',
  styleUrls: ['./firing-board.component.scss']
})
export class FiringBoardComponent implements OnInit, OnChanges {

  tiles: Tile[] = [];

  constructor() { }

  ngOnInit(): void {
    for (let x = 0; x < 11; x++) {
      for (let y = 0; y < 11; y++) {
        if (x === 0 && x < 11 || y === 0) {
          if (x === 0) {
            if (y > 0) {
              this.tiles[x * 11 + y] = new Tile(TileType.Empty, (y).toString());
            } else {
              this.tiles[x * 11 + y] = new Tile(TileType.Empty);
            }
          }
          else
            if (y === 0) {
              if (x > 0) {
                this.tiles[x * 11 + y] = new Tile(TileType.Empty, (x).toString());
              }
              else {
                this.tiles[x * 11 + y] = new Tile(TileType.Empty);
              }
            }

        }
        else {
          this.tiles[x * 11 + y] = new Tile(TileType.Empty);
        }
      }
    }
  }



  ngOnChanges(changes: SimpleChanges): void {
  }

  public shoot(shot: Shot): void {

    this.tiles[(shot.point.x + 1) * 11 + shot.point.y + 1].Type = shot.shotType === 0 ? ShotType.Miss : ShotType.Hit;
  }
}
