import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FiringBoardComponent } from './firing-board.component';

describe('FiringBoardComponent', () => {
  let component: FiringBoardComponent;
  let fixture: ComponentFixture<FiringBoardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FiringBoardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FiringBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
