export class TileType {
    public static readonly Empty = 0;
    public static readonly Sea = 1;
    public static readonly Ship = 2;
}
