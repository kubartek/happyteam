export class ShotType {
    public static readonly Miss = 1;
    public static readonly Hit = 2;
}
