import { Point } from './point';

export class Shot {
    public shotType!: number;
    public point: Point = new Point();
}
