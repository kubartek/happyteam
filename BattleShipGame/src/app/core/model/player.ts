import { Point } from './point';
import { Shot } from './shot';

export class Player {
    public ships: Point[] = [];
    public shots: Shot[] = [];
}
