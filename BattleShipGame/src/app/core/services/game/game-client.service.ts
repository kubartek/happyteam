import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { BehaviorSubject, ReplaySubject, EMPTY, Subject, of, Observable, from } from 'rxjs';
import { catchError, switchMap, tap } from 'rxjs/operators';
import { API_URL } from 'src/app/core/tokens';
import { Player } from '../../model/player';

@Injectable({
  providedIn: 'root'
})
export class GameClientService {
  public query$ = new BehaviorSubject(null);

  public result$ = new Subject<Player[]>();

  constructor(private http: HttpClient,
              @Inject(API_URL) private apiUrl: string) {
    this.query$.pipe(
      switchMap(query => this.makeRequest().pipe(
        catchError(error => {
          console.log(error); return EMPTY;
        })
      )),
    ).subscribe(this.result$);

  }

  getGame(): void {
    this.query$.next(null);
  }

  makeRequest(): Observable<Player[]> {
    const result = this.http.get<Player[]>(
      `${this.apiUrl}/api/BattleShipGame`);
    return result;
  }
}

