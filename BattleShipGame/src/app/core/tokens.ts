import { InjectionToken } from '@angular/core';
import { Player } from './model/player';

export const API_URL = new InjectionToken<Player[]>('API_URL - REST API ROOT URL');
