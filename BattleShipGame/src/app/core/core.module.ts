import { HttpClientModule } from '@angular/common/http';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { environment } from 'src/environments/environment';
import { API_URL } from './tokens';

@NgModule({
  imports: [
    HttpClientModule
  ],
  providers: [
    {
      provide: API_URL,
      useValue: environment.api_url
    },
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() core: CoreModule) {
    if (core !== null) {
      throw Error('Core should be included only ONCE in root module');
    }
  }
}
