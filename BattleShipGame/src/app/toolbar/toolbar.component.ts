import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { GameClientService } from '../core/services/game/game-client.service';
import { GameService } from '../game/game.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  public isGameInProgress = this._gameService.isGameInProgress$.asObservable();
  public summary = this._gameService.summary$.asObservable();

  constructor(private _gameService: GameService,
              private _gameClientService: GameClientService) {
  }

  ngOnInit(): void {
  }

  public newGameClick(): void {
    this._gameClientService.getGame();
  }

}
