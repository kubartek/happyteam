import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { TileType } from '../core/consts/TileType';
import { Point } from '../core/model/point';
import { Tile } from '../core/model/tile';


@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnChanges {

  constructor() { }

  @Input() board: Point[] = [];
  tiles: Tile[] = [];

  ngOnChanges(changes: SimpleChanges): void {

    if (this.board != null) {
      for (let x = 0; x < 11; x++) {
        for (let y = 0; y < 11; y++) {
          if (x === 0 && x < 11 || y === 0) {
            if (x === 0) {
              if (y > 0) {
                this.tiles[x * 11 + y] = new Tile(TileType.Empty, (y).toString());
              } else {
                this.tiles[x * 11 + y] = new Tile(TileType.Empty);
              }
            }
            else
              if (y === 0) {
                if (x > 0) {
                  this.tiles[x * 11 + y] = new Tile(TileType.Empty, (x).toString());
                }
                else {
                  this.tiles[x * 11 + y] = new Tile(TileType.Empty);
                }
              }
          } else {
            this.tiles[x * 11 + y] = new Tile(this.board.some(p => p.x === x - 1 && p.y === y - 1) === true ? TileType.Ship : TileType.Sea);
          }
        }
      }
    }
  }

}

