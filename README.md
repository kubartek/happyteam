# BattleShipGame

It is an angular application that simulates battleship gameplay between 2 players.
It requires connection string to BattleShipGameApi configuration in environment.ts

export const environment = {
  ( ... )
  api_url: 'https://localhost:44337',
};

# BattleShipGameApi

This is .Net 5.0 WebApi project that allows to get a complete game of Battleships.
A game consists of two players Boards with randomly placed ships and a list of shots that allow to sink all oponents ships.
A player who needs less shots to sink all oponents ships wins.

# BattleShipLib

This project is a library that:
- generates Battleship boards with randomly placed ships
- generates list of shots to a board based on a provided strategy until all shpis are sunk

# BattleShipLibTests

This project contains BattleShipLib tests.