﻿using BattleShipGameApi.Model;
using BattleShipLib;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BattleShipGameApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BattleShipGameController : ControllerBase
    {

        private readonly IFiringStrategy _firingStrategy;
        private readonly IBoardGenerator _boardGenerator;

        public BattleShipGameController(IFiringStrategy firingStrategy,
                                        IBoardGenerator boardGenerator)
        {
            _firingStrategy = firingStrategy;
            _boardGenerator = boardGenerator;
        }

        // GET: api/<BattleShipGameController>
        [HttpGet]
        public IEnumerable<Player> Get()
        {
            var board1 = _boardGenerator.GenerateRandomBoard();
            var board2 = _boardGenerator.GenerateRandomBoard();

            Player player1 = new(board1.GetShips().SelectMany(ship => ship.GetPoints()).ToList(), _firingStrategy.Solve(board2));
            Player player2 = new(board2.GetShips().SelectMany(ship => ship.GetPoints()).ToList(), _firingStrategy.Solve(board1));

            return new List<Player>() { player1, player2 };
        }

    }
}
