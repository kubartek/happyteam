﻿# BattleShipGameApi

This is .Net 5.0 WebApi project that allows to get a complete game of Battleships.
A game consists of two players Boards with randomly placed ships and a list of shots that allow to sink all oponents ships.
A player who needs less shots to sink all oponents ships wins.

