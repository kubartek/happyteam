﻿using BattleShipLib;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BattleShipGameApi.Model
{
    public class Player
    {

        public Player(List<Point> ships, List<IShot> shots) {
            Ships = ships;
            Shots = shots;
        }

        [Required]
        public List<Point> Ships { get; set; }
        [Required]
        public List<IShot> Shots { get; set; }
    }
}
