﻿using BattleShipLib;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace BattleshipLibTests
{
    class BoardGeneratorTests
    {

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void BoardGeneratorShipCountTest()
        {
            //Arrange
            var mockRandom = MockRandomNumberForBoardGenerator();
            IBoardGenerator generator = new BoardGenerator(mockRandom.Object);

            //Act
            IBoard board = generator.GenerateRandomBoard();

            //Assert
            Assert.IsTrue(board.GetShips().Count() == 5);
        }

        [Test]
        public void BoardGeneratorShipsLengthsTest()
        {
            //Arrange
            var mockRandom = MockRandomNumberForBoardGenerator();
            IBoardGenerator generator = new BoardGenerator(mockRandom.Object);
            List<int> expectedLengths = ShipConsts.ShipLength.GetShipLengths();

            //Act
            IBoard board = generator.GenerateRandomBoard();
            var orderedShipLengths = board.GetShips().Select(ship => ship.GetPoints().Count).OrderBy(x => x).Reverse().ToList<int>();

            //Assert
            Assert.AreEqual(expectedLengths, orderedShipLengths);
        }

        [Test]
        public void BoardGeneratorShipsInRangeTest()
        {
            //Arrange
            var mockRandom = MockRandomNumberForBoardGenerator();
            IBoardGenerator generator = new BoardGenerator(mockRandom.Object);
            HashSet<Point> allPoints = new();

            //Act
            IBoard board = generator.GenerateRandomBoard();
            foreach (var ship in board.GetShips())
            {
                allPoints.UnionWith(ship.GetPoints());
            }

            //Assert
            Assert.IsTrue(!allPoints.Any(point => point.X >= BoardConsts.Size || point.X >= BoardConsts.Size));
        }

        private static Mock<IMyRandom> MockRandomNumberForBoardGenerator() {
            Mock<IMyRandom> mock = new();
            Mock<IMyRandom> mockRandom = mock;
            mockRandom.SetupSequence(rand => rand.Next(2))
                .Returns(0)
                .Returns(0)
                .Returns(1)
                .Returns(0)
                .Returns(1)
                .Returns(1)
                .Returns(0)
                .Returns(0)
                .Returns(1)
                .Returns(1)
                .Returns(1)
                .Returns(1)
                .Returns(1)
                .Returns(1);
            mockRandom.SetupSequence(rand => rand.Next(5))
               .Returns(1);
            mockRandom.SetupSequence(rand => rand.Next(6))
                .Returns(2)
                .Returns(1)
                .Returns(3)
                .Returns(3);
            mockRandom.SetupSequence(rand => rand.Next(7))
                .Returns(5)
                .Returns(1)
                .Returns(4)
                .Returns(2)
                .Returns(5)
                .Returns(5)
                .Returns(0);
            mockRandom.SetupSequence(rand => rand.Next(8))
                .Returns(2)
                .Returns(6);
            mockRandom.SetupSequence(rand => rand.Next(10))
                .Returns(4)
                .Returns(3)
                .Returns(5)
                .Returns(8)
                .Returns(4)
                .Returns(4)
                .Returns(1)
                .Returns(2)
                .Returns(2)
                .Returns(5)
                .Returns(1)
                .Returns(9)
                .Returns(3);
            return mockRandom;
        }
    }
}
