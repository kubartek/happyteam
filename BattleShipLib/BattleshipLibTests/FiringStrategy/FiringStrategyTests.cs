﻿using BattleShipLib;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace BattleshipLibTests
{
    class FiringStrategyTests
    {

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void FiringBoardSolveTestAreEqual()
        {
            //Arrange
            IBoard board = CreateBoard();
            List<IShot> ExpectedShotsList = CreateExpectedShots();
            //Act
            FiringStrategy strategy = new();
            List<IShot> ActualShotsList = strategy.Solve(board);

            //Assert
            Assert.AreEqual(ExpectedShotsList, ActualShotsList);
        }

        [Test]
        public void FiringBoardSolveTestAreNotEqual()
        {
            //Arrange
            IBoard board = CreateBoard();
            List<IShot> ExpectedShotsList = CreateExpectedShots();
            ExpectedShotsList[0].ShotType = ShotConsts.ShotType.Hit;
            //Act
            FiringStrategy strategy = new();
            List<IShot> ActualShotsList = strategy.Solve(board);

            //Assert
            Assert.AreNotEqual(ExpectedShotsList, ActualShotsList);
        }

        private static IBoard CreateBoard()
        {
            Board board = new();

            IShip ship1 = new Ship();
            ship1.SetPoints(new HashSet<Point>() {

                new Point() {
                    X =  0,
                    Y =  2
                }, new Point(){
                    X= 1,
                    Y= 2
                }, new Point(){
                    X= 2,
                    Y= 2
                }, new Point(){
                    X= 3,
                    Y= 2
                }, new Point(){
                    X= 4,
                    Y= 2
                }

            });

            IShip ship2 = new Ship();
            ship2.SetPoints(new HashSet<Point>(){
             new Point(){
                    X= 4,
                    Y= 8
                },new Point() {
                    X= 5,
                    Y= 8
                }, new Point(){
                    X= 6,
                    Y= 8
                }, new Point(){
                    X= 7,
                    Y= 8
                }
            });

            IShip ship3 = new Ship();
            ship3.SetPoints(new HashSet<Point>(){
             new Point(){
                    X= 6,
                    Y= 4
                }, new Point(){
                    X= 7,
                    Y= 4
                },new Point() {
                    X= 8,
                    Y= 4
                }
            });

            IShip ship4 = new Ship();
            ship4.SetPoints(new HashSet<Point>(){
             new Point(){
                    X= 4,
                    Y= 4
                }, new Point(){
                    X= 4,
                    Y= 5
                }, new Point(){
                    X= 4,
                    Y= 6
                }
            });

            IShip ship5 = new Ship();
            ship5.SetPoints(new HashSet<Point>(){
             new Point(){
                    X= 5,
                    Y= 0
                }, new Point() {
                    X= 6,
                    Y= 0
                }
            });


            List<IShip> ships = new() {
                ship1,
                ship2,
                ship3,
                ship4,
                ship5
            };

            board.SetShips(ships);
            return board;

        }

        private static List<IShot> CreateExpectedShots()
        {

            List<IShot> shots = new()
            {
                new Shot(new Point(0,0), (ShotConsts.ShotType) 0),
                new Shot(new Point(0,2), (ShotConsts.ShotType) 1),
                new Shot(new Point(0,1), (ShotConsts.ShotType) 0),
                new Shot(new Point(1,2), (ShotConsts.ShotType) 1),
                new Shot(new Point(2,2), (ShotConsts.ShotType) 1),
                new Shot(new Point(3,2), (ShotConsts.ShotType) 1),
                new Shot(new Point(4,2), (ShotConsts.ShotType) 1),
                new Shot(new Point(0,4), (ShotConsts.ShotType) 0),
                new Shot(new Point(0,6), (ShotConsts.ShotType) 0),
                new Shot(new Point(0,8), (ShotConsts.ShotType) 0),
                new Shot(new Point(1,1), (ShotConsts.ShotType) 0),
                new Shot(new Point(1,3), (ShotConsts.ShotType) 0),
                new Shot(new Point(1,5), (ShotConsts.ShotType) 0),
                new Shot(new Point(1,7), (ShotConsts.ShotType) 0),
                new Shot(new Point(1,9), (ShotConsts.ShotType) 0),
                new Shot(new Point(2,0), (ShotConsts.ShotType) 0),
                new Shot(new Point(2,4), (ShotConsts.ShotType) 0),
                new Shot(new Point(2,6), (ShotConsts.ShotType) 0),
                new Shot(new Point(2,8), (ShotConsts.ShotType) 0),
                new Shot(new Point(3,1), (ShotConsts.ShotType) 0),
                new Shot(new Point(3,3), (ShotConsts.ShotType) 0),
                new Shot(new Point(3,5), (ShotConsts.ShotType) 0),
                new Shot(new Point(3,7), (ShotConsts.ShotType) 0),
                new Shot(new Point(3,9), (ShotConsts.ShotType) 0),
                new Shot(new Point(4,0), (ShotConsts.ShotType) 0),
                new Shot(new Point(4,4), (ShotConsts.ShotType) 1),
                new Shot(new Point(4,3), (ShotConsts.ShotType) 0),
                new Shot(new Point(3,4), (ShotConsts.ShotType) 0),
                new Shot(new Point(5,4), (ShotConsts.ShotType) 0),
                new Shot(new Point(4,5), (ShotConsts.ShotType) 1),
                new Shot(new Point(4,6), (ShotConsts.ShotType) 1),
                new Shot(new Point(4,7), (ShotConsts.ShotType) 0),
                new Shot(new Point(4,8), (ShotConsts.ShotType) 1),
                new Shot(new Point(3,8), (ShotConsts.ShotType) 0),
                new Shot(new Point(5,8), (ShotConsts.ShotType) 1),
                new Shot(new Point(6,8), (ShotConsts.ShotType) 1),
                new Shot(new Point(7,8), (ShotConsts.ShotType) 1),
                new Shot(new Point(8,8), (ShotConsts.ShotType) 0),
                new Shot(new Point(5,1), (ShotConsts.ShotType) 0),
                new Shot(new Point(5,3), (ShotConsts.ShotType) 0),
                new Shot(new Point(5,5), (ShotConsts.ShotType) 0),
                new Shot(new Point(5,7), (ShotConsts.ShotType) 0),
                new Shot(new Point(5,9), (ShotConsts.ShotType) 0),
                new Shot(new Point(6,0), (ShotConsts.ShotType) 1),
                new Shot(new Point(5,0), (ShotConsts.ShotType) 1),
                new Shot(new Point(7,0), (ShotConsts.ShotType) 0),
                new Shot(new Point(6,2), (ShotConsts.ShotType) 0),
                new Shot(new Point(6,4), (ShotConsts.ShotType) 1),
                new Shot(new Point(6,3), (ShotConsts.ShotType) 0),
                new Shot(new Point(7,4), (ShotConsts.ShotType) 1),
                new Shot(new Point(8,4), (ShotConsts.ShotType) 1)

            };

            return shots;

        }
    }
}
