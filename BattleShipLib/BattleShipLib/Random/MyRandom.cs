﻿using System;

namespace BattleShipLib
{
    public class MyRandom : IMyRandom
    {
        private readonly Random rand = new(Guid.NewGuid().GetHashCode());
        public int Next(int max)
        {
            int r = rand.Next(max);
            return r;
        }
    }
}
