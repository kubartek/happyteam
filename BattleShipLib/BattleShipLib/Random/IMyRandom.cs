﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShipLib
{
    public interface IMyRandom
    {
        public int Next(int max);
    }
}
