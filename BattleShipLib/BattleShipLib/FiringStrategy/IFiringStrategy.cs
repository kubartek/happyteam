﻿using System.Collections.Generic;

namespace BattleShipLib
{
    public interface IFiringStrategy
    {
        public Point NextShot(IFiringBoard firingBoard);
        public List<IShot> Solve(IBoard board);
    }
}
