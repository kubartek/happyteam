﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BattleShipLib
{
    public class FiringStrategy : IFiringStrategy
    {
        public Point NextShot(IFiringBoard firingBoard)
        {
            List<IShip> ships = new();
            List<IShot> shots = firingBoard.GetShots();

            List<Point> hits = shots.Where(sh => sh.ShotType == ShotConsts.ShotType.Hit).Select(hit => hit.Point ).ToList();
            List<Point> misses = shots.Where(sh => sh.ShotType == ShotConsts.ShotType.Miss).Select(miss => miss.Point).ToList();

            foreach (Point hit in hits)
            {
                if (ships.Any(ship => ship.GetPoints().Contains(hit)))
                {
                    continue;
                }

                Ship ship = new();
                ship.SetPoints(new HashSet<Point>(GetAdjacentHits(hit, hits)));
                ships.Add(ship);
            }

            foreach (Ship ship in ships)
            {
                List<Point> points = ship.GetPoints().ToList();

                if (points.Count == 5) continue;

                if (points.Count == 1)
                {
                    return AdjacentShot(points[0], misses);
                }

                int orientation = GetShipOrientation(points);
                var orderedPoints = points.OrderBy(p => orientation == 0 ? p.X : p.Y);

                if (orientation == 0)
                {
                    var start = orderedPoints.First();
                    if (start.X - 1 >= 0)
                    {
                        Point beforeStart = new(start.X - 1, start.Y);
                        if (!misses.Contains(beforeStart))
                        {
                            return beforeStart;
                        }
                    }
                    var end = orderedPoints.Last();
                    if (end.X + 1 < BoardConsts.Size)
                    {
                        Point afterEnd = new(end.X + 1, end.Y);
                        if (!misses.Contains(afterEnd))
                        {
                            return afterEnd;
                        }
                    }
                }
                else
                {
                    Point start = orderedPoints.First();

                    if (start.Y - 1 >= 0)
                    {
                        Point beforeStart = new(start.X, start.Y - 1);
                        if (!misses.Contains(beforeStart))
                        {
                            return beforeStart;
                        }
                    }
                    var end = orderedPoints.Last();
                    if (end.Y + 1 < BoardConsts.Size)
                    {
                        Point afterEnd = new(end.X, end.Y + 1);
                        if (!misses.Contains(afterEnd))
                        {
                            return afterEnd;
                        }
                    }
                }

            }

            return GetPseudoRandomShot(firingBoard);
        }

        public List<IShot> Solve(IBoard board)
        {
            IFiringBoard firingBoard = new FiringBoard();
            while (!firingBoard.GetStatus())
            {
                Point shot = NextShot(firingBoard);
                if (board.CheckPoint(shot))
                {
                    firingBoard.AddShot(new Shot(shot, ShotConsts.ShotType.Hit));
                }
                else
                {
                    firingBoard.AddShot(new Shot(shot, ShotConsts.ShotType.Miss));
                }
            }

            return firingBoard.GetShots();
        }

        protected static Point GetPseudoRandomShot(IFiringBoard firingBoard)
        {

            for (int i = 0; i < BoardConsts.Size; i++)
                for (int j = 0; j < BoardConsts.Size; j++)
                    if ((i % 2 == 0 && j % 2 == 0)
                        || (i % 2 == 1 && j % 2 == 1))
                    {
                        Point point = new(i, j);
                        if (!firingBoard.GetShots().Any(sh => sh.Point.X == i && j == sh.Point.Y))
                        {
                            return point;
                        }
                    }

            return new Point(0, 0);

        }

        protected static int GetShipOrientation(IEnumerable<Point> points)
        {
            if (points.Count() >= 2)
            {
                Point p1 = points.First();
                Point p2 = points.Skip(1).Take(1).First();

                if (p1.X == p2.X) return 1;
                else return 0;
            }

            throw new Exception("Cannot calculate orientation if points count is below 2.");
        }

        protected static Point AdjacentShot(Point point, IEnumerable<Point> misses)
        {
            Point point1 = new(point.X, point.Y - 1);
            if (point.Y - 1 >= 0 && !misses.Contains(point1))
                return point1;

            Point point2 = new(point.X - 1, point.Y);
            if (point.X - 1 >= 0 && !misses.Contains(point2))
                return point2;

            Point point3 = new(point.X + 1, point.Y);
            if (point.X + 1 < BoardConsts.Size && !misses.Contains(point3))
                return point3;

            return new Point(point.X, point.Y + 1);
        }

        protected List<Point> GetAdjacentHits(Point hit, IEnumerable<Point> hits)
        {
            List<Point> adjacentPoints = new();
            if (hit.Y - 1 >= 0) adjacentPoints.Add(new Point(hit.X, hit.Y - 1));
            if (hit.X - 1 >= 0) adjacentPoints.Add(new Point(hit.X - 1, hit.Y));
            if (hit.X + 1 < BoardConsts.Size) adjacentPoints.Add(new Point(hit.X + 1, hit.Y));
            if (hit.Y + 1 < BoardConsts.Size) adjacentPoints.Add(new Point(hit.X, hit.Y + 1));

            List<Point> directlyAdjacentHits = adjacentPoints.Intersect(hits).ToList();
            List<Point> adjacentHits = new() { hit };

            foreach (Point directlyAdjacentHit in directlyAdjacentHits)
            {
                adjacentHits = adjacentHits.Union(GetAdjacentHits(directlyAdjacentHit, hits.Where(h => (h.X != hit.X || h.Y != hit.Y) && !directlyAdjacentHits.Contains(h)))).ToList();
            }

            return adjacentHits.Union(directlyAdjacentHits).ToList();

        }


    }
}
