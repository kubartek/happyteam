﻿using System.Collections.Generic;

namespace BattleShipLib
{
    public interface IBoard
    {
        public IEnumerable<IShip> GetShips();
        public void SetShips(IEnumerable<IShip> ships);

        public bool CheckPoint(Point point);
    }
}
