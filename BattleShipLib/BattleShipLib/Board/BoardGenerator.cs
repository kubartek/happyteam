﻿using System;
using System.Collections.Generic;

namespace BattleShipLib
{
    public class BoardGenerator : IBoardGenerator
    {
        private readonly IMyRandom rand;

        private BoardGenerator() { }

        public BoardGenerator(IMyRandom random) {
            this.rand = random;
        }

        public IBoard GenerateRandomBoard()
        {

            var ships = new List<IShip>();
            foreach (int length in ShipConsts.ShipLength.GetShipLengths())
            {
                Boolean isSuccess = false;
                while (!isSuccess)
                {

                    int orientation = rand.Next(2);
                    Point randomPoint = GetRandomPoint(length, orientation);

                    if (!CheckIfShipFits(randomPoint, length, orientation)) continue;

                    HashSet<Point> points = new();
                    bool isCollision = false;

                    for (int i = 0; i < length; i++)
                    {
                        Point point = GetCalculatedPoint(randomPoint, i, orientation);
                        foreach (var ship in ships)
                        {

                            if (ship.CheckPoint(point, true))
                            {
                                isCollision = true;
                                break;
                            }
                        }
                        points.Add(point);
                    }

                    if (!isCollision)
                    {
                        var ship = new Ship();
                        ship.SetPoints(points);
                        ships.Add(ship);
                        isSuccess = true;
                    }
                }

            }

            var board = new Board();
            board.SetShips(ships);
            return board;

        }

        protected Point GetRandomPoint(int length, int orientation)
        {
            return new Point(orientation == 0 ? rand.Next(BoardConsts.Size - length) : rand.Next(BoardConsts.Size),
                orientation == 0 ? rand.Next(BoardConsts.Size) : rand.Next(BoardConsts.Size - length));
        }

        protected static Point GetCalculatedPoint(Point point, int length, int orientation)
        {

            return new Point(orientation == 0 ? point.X + length : point.X,
                    orientation == 0 ? point.Y : point.Y + length);
        }

        protected static bool CheckIfShipFits(Point point, int length, int orientation)
        {
            if (orientation == 0)
            {
                if (point.X + length > BoardConsts.Size - 1) return false;
            }
            else if (orientation == 1)
            {
                if (point.Y + length > BoardConsts.Size - 1) return false;
            }
            return true;
        }


    }
}
