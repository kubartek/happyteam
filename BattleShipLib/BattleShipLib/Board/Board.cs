﻿using System.Collections.Generic;

namespace BattleShipLib
{
    public class Board : IBoard
    {
        private List<IShip> Ships { get; set; }
        public IEnumerable<IShip> GetShips() => Ships;
        public void SetShips(IEnumerable<IShip> ships)
        {
            this.Ships = ships as List<IShip>;
        }

        public bool CheckPoint(Point point)
        {
            foreach (IShip ship in Ships)
            {
                if (ship.CheckPoint(point))
                    return true;
            }
            return false;
        }
    }
}
