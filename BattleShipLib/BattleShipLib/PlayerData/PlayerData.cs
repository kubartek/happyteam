﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShipLib
{
    public class PlayerData
    {
        public IBoard Board { get; set; }
        public IFiringBoard FiringBoard { get; set; }
        public IFiringStrategy FiringStrategy { get; set; }
    }
}
