﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BattleShipLib
{
    public class FiringBoard : IFiringBoard
    {
        private readonly List<IShot> shots = new();

        public void AddShot(IShot shot)
        {
            if (!shots.Any(s => s.Point.X == shot.Point.X && s.Point.Y == shot.Point.Y))
                shots.Add(shot);
            else
            {
                throw new Exception("You have already shot this point!");
            }
        }

        public List<IShot> GetShots() => shots;

        public bool GetStatus()
        {
           return shots.Where(sh => sh.ShotType == ShotConsts.ShotType.Hit).Count() == ShipConsts.ShipLength.GetShipLengths().Sum();
        }
    }
}
