﻿using System.Collections.Generic;

namespace BattleShipLib
{
    public interface IFiringBoard
    {
        public void AddShot(IShot shot);
        public List<IShot> GetShots();
        public bool GetStatus();
    }
}
