﻿using System;

namespace BattleShipLib
{
    public class Shot : IShot ,IEquatable<Shot>
    { 
        public Shot(Point point, ShotConsts.ShotType type)
        {
            Point = point;
            ShotType = type;
        }
        public Point Point { get; set; }

        public ShotConsts.ShotType ShotType { get; set; }

        public bool Equals(Shot other)
        {
            if (this.ShotType != other.ShotType)
                return false;
            else return this.Point.Equals(other.Point);
        }
    }
}
