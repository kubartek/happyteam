﻿namespace BattleShipLib
{
    public interface IShot
    {
        public ShotConsts.ShotType ShotType { get; set; }
        public Point Point { get; set; }
    }
}
