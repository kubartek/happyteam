﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShipLib
{
    public static class ShipConsts
    {
        public static class ShipLength
        {
            static readonly int Carrier = 5;
            static readonly int Battleship = 4;
            static readonly int Destroyer = 3;
            static readonly int Submarine = 3;
            static readonly int PatrolBoat = 2;

            public static List<int> GetShipLengths() { 
                return new List<int> { Carrier, Battleship, Destroyer, Submarine, PatrolBoat };
            }
        }
    }
}
