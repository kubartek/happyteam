﻿using System;
using System.Collections.Generic;

namespace BattleShipLib
{
    public class Ship : IShip
    {
        private readonly HashSet<Point> points = new();

        public bool CheckPoint(Point point, Boolean checkSurroundings = false)
        {
             if (points.Contains(point))
            {
                return true;
            }

            if (checkSurroundings)
            {
                return CheckPointSurroundings(point);
            }

            return false;
        }

        protected bool CheckPointSurroundings(Point point)
        {
            var surroundingPoints = new HashSet<Point>() {
                new Point(point.X - 1, point.Y -1), new Point(point.X, point.Y-1),  new Point(point.X+1, point.Y-1),
                new Point(point.X - 1, point.Y),                                    new Point(point.X+1, point.Y),
                new Point(point.X - 1, point.Y + 1), new Point(point.X, point.Y + 1),  new Point(point.X+1, point.Y+1),
            };

            if (points.Overlaps(surroundingPoints))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public void SetPoints(HashSet<Point> points)
        {
            this.points.UnionWith(points);
        }

        public HashSet<Point> GetPoints()
        {
            return points;
        }
    }
}
