﻿using System;
using System.Collections.Generic;

namespace BattleShipLib
{
    public interface IShip
    {
        bool CheckPoint(Point point, Boolean checkSurroundings = false);
        void SetPoints(HashSet<Point> points);
        HashSet<Point> GetPoints();
    }
}
